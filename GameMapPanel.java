import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Timer;

public class GameMapPanel extends Panel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Karta karta;
	static int broqch = 0;
	int sila1 = 0;
	int sila2 = 0;
	Timer time1;
	Timer time2;
	GameMapPanel(Karta k) {
		
		
		karta = k;
		GameKeyListener gkl = new GameKeyListener(this);
		GameMouseListener gml = new GameMouseListener(this);
		setLayout(null);
		addKeyListener(gkl);
		addMouseListener(gml);
		
        time1 = new Timer(200,new ActionListener() {
		    

			@Override
			public void actionPerformed(ActionEvent e) {
				broqch++;
				sila1++;
				if(broqch==20)
				{
                karta.spawncoin();
                repaint();
                broqch=0;
				}
				
			}
		});
         time2 = new Timer(200,new ActionListener() {
		    

			@Override
			public void actionPerformed(ActionEvent e) {
				broqch++;
				sila2++;
				if(broqch==20)
				{
                karta.spawncoin();
                repaint();
                broqch=0;
				}
				
			}
		});
	}
	
	public void paint(Graphics g) {
		
        File m = new File("gas.png");
		try {
			BufferedImage bi = ImageIO.read(m);
			g.drawImage(bi, karta.gas.getPlacex1() * 60, karta.gas.getPlacey1() * 60, 60, 60, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		File f = new File("car.png");
		try {
			BufferedImage bi = ImageIO.read(f);
			System.out.println(karta.hero1.getPosx()+" "+karta.hero1.getPosy());
			g.drawImage(bi, karta.hero1.getPosx() * 60, karta.hero1.getPosy() * 60, 60, 60, null);
			g.drawImage(bi, karta.hero2.getPosx() * 60, karta.hero2.getPosy() * 60, 60, 60, null);
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		  if(karta.hero1.getPosx()==karta.gas.getPlacex1() && karta.hero1.getPosy()==karta.gas.getPlacey1() || karta.hero1.getPosx()==karta.gas.getPlacex1() && karta.hero1.getPosy()==karta.gas.getPlacey1()) {
			  time1.start();
	      }
		  else time1.stop();
		  if(karta.hero2.getPosx()==karta.gas.getPlacex1() && karta.hero2.getPosy()==karta.gas.getPlacey1() || karta.hero2.getPosx()==karta.gas.getPlacex1() && karta.hero2.getPosy()==karta.gas.getPlacey1()) {
			  time2.start();
	      }
		  else time2.stop();
		  if(karta.hero1.getPosx() == karta.hero2.getPosx() && karta.hero1.getPosy() == karta.hero2.getPosy())
		  {
			  karta.crash();
		  }
		
		int sizexInPixels = karta.sizex * 60;
		int sizeyInPixels = karta.sizey * 60;
		
		for (int i = 0; i <= karta.sizex; i++) {
			g.drawLine(i * 60, 0, i * 60, sizeyInPixels);
		}
		for (int i = 0; i <= karta.sizey; i++) {
			g.drawLine(0, i * 60, sizexInPixels, i * 60);
		}
	}
}
