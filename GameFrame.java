import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

public class GameFrame extends Frame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	GameMapPanel mapPanel;
	Label label;
	Label label1;
	Label label2;
	Label label3;
	Label label4;
	Label label5;
	Label label6;
	Label label7;
	Label label8;
	Label label9;
	Label label10;
	Label label11;
	Button button;
	TextField textField;
	TextArea textArea;

	GameFrame(Karta k) {
		GameWindowListener gwl = new GameWindowListener(this);
		addWindowListener(gwl);
		mapPanel = new GameMapPanel(k);
		mapPanel.setBounds(30, 30, 600, 625);
		
		button = new Button();
		button.setBounds(k.sizex*90, 30, 100, 30);
		button.setLabel("Информация");
		this.add(button);
		
		GameButtonActionListener al = new GameButtonActionListener(this);
		button.addActionListener(al);
		
		label = new Label();
		label.setBounds(k.sizex*90, 90, 600, 30);
		label.setText("");
		this.add(label);
		
		label2 = new Label();
		label2.setBounds(k.sizex*90, 130, 600, 30);
		label2.setText("Здравей играч номер Х !");
		this.add(label2);
		
		label3 = new Label();
		label3.setBounds(k.sizex*90, 150, 800, 30);
		label3.setText("Изгради репутация си на най-добрия състезател на улицата в подземния свят на Need For Speed и надхитри полицията,");
		label.setVisible(true);
		this.add(label3);
		
		label4 = new Label();
		label4.setBounds(k.sizex*90, 170, 800, 30);
		label4.setText("която дебне зад всеки ъгъл.Need For Speed предлага огромен набор от възможности за персонализиране на колите, за да ");
		this.add(label4);
		
		label5 = new Label();
		label5.setBounds(k.sizex*90, 190, 800, 30);
		label5.setText(" бъдеш наистина впечатляващ и разпознаваем на пътя.Модифицирай двигателя, спирачките, окачването, гумите и промени ");
		label5.setVisible(true);
		this.add(label5);
	
		label6 = new Label();
		label6.setBounds(k.sizex*90, 210, 800, 30);
		label6.setText("външния вид на своя автомобил според предпочитанията си.Тук не говорим за пари, а репутация. Колкото по – бърз си и ");
		label6.setVisible(true);
		this.add(label6);
		
		label7 = new Label();
		label7.setBounds(k.sizex*90, 230, 800, 30);
		label7.setText("успяваш да се измъкнеш от полицията, толкова по известен и търсен ставаш.Играта започва, когато всички са на началната ");
		label7.setVisible(true);
		this.add(label7);
	
		label8 = new Label();
		label8.setBounds(k.sizex*90, 250, 800, 30);
		label8.setText("линия и тръгнат при сигнал. Освен, че се бориш за първо място трябва да се пазиш от полицията, хванат ли те играта свършва.");
		label8.setVisible(true);
		this.add(label8);
		
		label9 = new Label();
		label9.setBounds(k.sizex*90, 270, 800, 30);
		label9.setText("Можеш да започнеш отначало следващата вечер, а до тогава да обмислиш стратегиите си и да подобриш колата си в гаража. ");
		label9.setVisible(true);
		this.add(label9);
		
		label10 = new Label();
		label10.setBounds(k.sizex*90, 290, 600, 30);
		label10.setText("Успех и не забравяй да се забавляваш !");
		label10.setVisible(true);
		this.add(label10);
		
//Енергия
		label1 = new Label();
		label1.setBounds(k.sizex*90, 310, 100, 30);
		label1.setText("Fuel Car1: " + mapPanel.sila1);
		label1.setVisible(true);
		this.add(label1);
		
		label11 = new Label();
		label11.setBounds(k.sizex*90+100, 310, 100, 30);
		label11.setText("Fuel Car2:" + mapPanel.sila2);
		label11.setVisible(true);
		this.add(label11);
//		timer
		Timer time = new Timer(50,new ActionListener() {
		    

			@Override
			public void actionPerformed(ActionEvent e) {
			
				label1.setText("Fuel Car1: " + mapPanel.sila1);
				label11.setText("Fuel Car2: " + mapPanel.sila2);
				if(mapPanel.karta.isCrashed())
				{
					label11.setVisible(false);
					if(mapPanel.sila1>mapPanel.sila2)label1.setText("PLAYER 1 WINS!!!");
					else if(mapPanel.sila1<mapPanel.sila2)label1.setText("PLAYER 2 WINS!!!");
					else label1.setText("ITS A TIE");
				}
			}
		});
		time.start();
		this.add(mapPanel);
	}
	

}
