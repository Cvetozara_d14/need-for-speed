import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class GameWindowListener implements WindowListener {
	GameFrame frame;
	
	GameWindowListener(GameFrame f) {
		frame = f;
	}
	
	@Override
	public void windowOpened(WindowEvent e) {
		System.out.println("Отвори се играта");
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// запис във файл на координатите на героя
		//System.out.println(frame.mapPanel.karta.hero.getPosx());
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}
	
}

