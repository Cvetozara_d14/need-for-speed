import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		// четене от файл
		File file = new File("data.txt");
		try {
			Scanner scan = new Scanner(file);
			int mapsizex = scan.nextInt();
			int mapsizey = scan.nextInt();
			int hero1posx = scan.nextInt();
			int hero1posy = scan.nextInt();
			int hero2posx = scan.nextInt();
			int hero2posy = scan.nextInt();
			// създавам картата и героя
			Karta k = new Karta(mapsizex, mapsizey);
			Hero h1 = new Hero(hero1posx, hero1posy);
			Hero h2 = new Hero(hero2posx, hero2posy);
			Gas m = new Gas();
			
			k.hero1 = h1;
			k.hero2 = h2;
			k.gas = m;
			
			GameFrame f = new GameFrame(k);
		
			f.setLayout(null);
			f.setSize(f.getMaximumSize());
			f.setVisible(true);
			k.Startgame(f.mapPanel);
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		// записване във файл
		File fileout = new File("zapis.txt");
		try {
			int variable = 20;
			FileWriter fw = new FileWriter(fileout);
			fw.write("" + variable);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
