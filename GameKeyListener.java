import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener implements KeyListener {
	GameMapPanel panel;
	
	GameKeyListener(GameMapPanel p) {
		panel = p;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}


	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(panel.karta.isCrashed())return;
		switch(e.getKeyCode()) {
		case KeyEvent.VK_W: panel.karta.hero1.setPosy(panel.karta.hero1.getPosy() - 1); break;
		case KeyEvent.VK_S: panel.karta.hero1.setPosy(panel.karta.hero1.getPosy() + 1); break;
		case KeyEvent.VK_A: panel.karta.hero1.setPosx(panel.karta.hero1.getPosx() - 1); break;
		case KeyEvent.VK_D: panel.karta.hero1.setPosx(panel.karta.hero1.getPosx() + 1); break;
		
		case KeyEvent.VK_UP: panel.karta.hero2.setPosy(panel.karta.hero2.getPosy() - 1); break;
		case KeyEvent.VK_DOWN: panel.karta.hero2.setPosy(panel.karta.hero2.getPosy() + 1); break;
		case KeyEvent.VK_LEFT: panel.karta.hero2.setPosx(panel.karta.hero2.getPosx() - 1); break;
		case KeyEvent.VK_RIGHT: panel.karta.hero2.setPosx(panel.karta.hero2.getPosx() + 1); break;
	}
	panel.repaint();
		
	}
}
